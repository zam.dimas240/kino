import React, { useState } from "react";
import styles from './Pagination.module.scss';
import {
  Link
} from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { incrementPageSmall, decrementPage, switchPage, incrementPage, incrementPageMax } from "../../store/cards/cards";


const pages = [1,2,3,4,5];

const Pagination: React.FC = () => {
  const dispatch = useDispatch()
  //@ts-ignore
  const {filter} = useSelector((state) => state.cards);
  return (
    <div className={styles.pagination__container}>
      <Link to={`/${filter._page}`} onClick={() => dispatch(incrementPageSmall())}>
        <button>&laquo;</button>
      </Link>
      <Link to={`/${filter._page}`} onClick={() => dispatch(decrementPage())}>
      <button>&laquo;</button>
      </Link>
      {pages.map((el, ind) => (
        <Link onClick={() => dispatch(switchPage(el))} to={`/${ind + 1}`}>
        <button>{el}</button>
        </Link>
      ))}
      <Link onClick={() => dispatch(incrementPage())} to={`/${filter._page}`}>
      <button>&raquo;</button>
      </Link>
      <Link onClick={() => dispatch(incrementPageMax())} to={`/${filter._page}`}>
      <button>&raquo;</button>
      </Link>
    </div> 
  )
};

export default Pagination;