import React, { useState } from "react";
import styles from './Header.module.scss';
import cn from 'classnames'

import Logo from '../../assets/icon/logo.png';
import Switch_color from '../../assets/icon/switch_color.png';;

const Header: React.FC = () => {

  return (
    <div className={styles.header__container}>
      <img src={Logo} alt="" />
      <img src={Switch_color} alt="" />
    </div> 
  )
};

export default Header;