import React from "react";
import styles from './CardItem.module.scss';

interface CardItemProps {
  title: string;
  created: string;
  image: string;
}

const CardItem: React.FC<CardItemProps> = ({ title, created, image}) => {

  return (
    <div className={styles.cardItem__container}>
      <img className={styles.cardItem__image} src={`https://test-front.framework.team/${image}`} alt={title} />
      <div className={styles.cardItem__title}>
        <p>{title}</p>
        <p>{created}</p>
      </div>
    </div>
  )
};

export default CardItem;