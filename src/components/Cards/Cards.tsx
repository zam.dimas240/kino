import React, { useEffect } from "react";
import styles from './Cards.module.scss';
import CardItem from "./CardItem";
import { useDispatch, useSelector } from "react-redux";
import { fetchAuthors, fetchCardsQuery, fetchLocations } from "../../store/cards/cards";

const Cards: React.FC = () => {
  const dispatch = useDispatch();
  //@ts-ignore
  const { cards, filter } = useSelector(state => state.cards );

  useEffect(() => {
    //@ts-ignore
    dispatch(fetchCardsQuery(filter));
  }, [filter])

  useEffect(() => {
    //@ts-ignore
    dispatch(fetchAuthors());
    //@ts-ignore
    dispatch(fetchLocations());
  }, [])

  return (
    <div className={styles.cards_container}>
      {cards.map((el: any) => (
         <CardItem title={el.name} created={el.created} image={el.imageUrl}/>
      ))}
    </div> 
  )
};

export default Cards;