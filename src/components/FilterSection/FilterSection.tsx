import React, { useState } from "react";
import styles from './FilterSection.module.scss';
import Select from "../ui/Select";
import Input from "../ui/Input";
import { useDispatch, useSelector } from "react-redux";
import { changeFilterAuthor, changeFilterLocation, changeFilterName } from "../../store/cards/cards";

const FilterSection: React.FC = () => {
  const dispatch = useDispatch()
  const [isAuthor, setAuthor] = useState('');
  const [isLocation, setLocation] = useState('');
  //@ts-ignore
  const { filter, locations, authors } = useSelector(state => state.cards);

  const setItemName = (event: any) => {
    if (event.target) {
      dispatch(changeFilterName(event.target.value))
    } else {
      dispatch(changeFilterName(null))
    }
  }
  const setItemAuthor = (el: any) => {
    if (authors.find((item: any) => item.id === el)) {
      setAuthor(authors.find((item: any) => item.id === el).name)
    } else {
      setAuthor('')
    }
    dispatch(changeFilterAuthor(el))
  }
  const setItemLocation = (el: any) => {
    if (locations.find((item: any) => item.id === el)) {
      setLocation(locations.find((item: any) => item.id === el).location)
    } else {
      setLocation('')
    }
    dispatch(changeFilterLocation(el))
  }

  return (
    <div className={styles.filterSection__container}>      
        <Input value={filter.name} placeholder='Name' onChange={setItemName}/>
        <Select value={isAuthor} onChange={setItemAuthor} placeholder='Author' items={authors}/>
        <Select value={isLocation} onChange={setItemLocation} placeholder='Location' items={locations}/>
    </div>
  )
};

export default FilterSection;