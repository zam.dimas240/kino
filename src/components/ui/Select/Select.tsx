import React, { useState } from "react";
import styles from './Select.module.scss';
import cn from 'classnames'

interface SelectProps {
  value: string
  onChange: (text: any) => void
  items: any
  placeholder: string
}

const Select: React.FC<SelectProps> = ({value, onChange, items, placeholder}) => {
  const [isOpen, setOpen] = useState<boolean>(false);

  const openSelect = () => {
    setOpen(!isOpen);
  }

  const changeItem = (el: any) =>{
      onChange(el);
      setOpen(false);
  }

  return (
    <div className={styles.select_container}>
      <div className={styles.select_input_block}>
        <input className={cn(styles.select_input, isOpen && styles.select_input_open)} placeholder={placeholder} type="text" value={value} onClick={openSelect}/>
        {value && <span className={styles.select_input_clear} onClick={() => changeItem(null)}>&#10008;</span>}
      </div>
      {isOpen && (
        <ul className={styles.select_menu}>
          {items.map((el: any, ind: any) => (
           <li key={ind} className={styles.select_menu_item} onClick={() => changeItem(el.id)}>
             {el.name ? el.name : el.location}
           </li>
          ))}
        </ul>
      )}
    </div> 
  )
};

export default Select;