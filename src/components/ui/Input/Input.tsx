import React from "react";
import styles from './Input.module.scss';

interface InputProps {
  value: string
  onChange: (text: any) => void
  placeholder: string
}

const Input: React.FC<InputProps> = ({value, onChange, placeholder}) => {
  return (
    <div className={styles.input_container}>
      <input className={styles.input_block} placeholder={placeholder} type="text" value={value ? value : ''} onChange={(e) => onChange(e)}/>
      {value && <span className={styles.input_input_clear} onClick={() => onChange('')}>&#10008;</span>}
    </div> 
  )
};

export default Input;