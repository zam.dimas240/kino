import qs from 'qs';

const urlApi = 'https://test-front.framework.team/';

export const cardsApi = {
  getCards: () => {
    return `${urlApi}paintings`;
  },

  getCardsQuery: (
    queryObj?: any
  ) => {
    const queryString = qs.stringify(queryObj, {skipNulls: true});
    return `${urlApi}paintings?${queryString}`;
  },

  getAuthors: () => {
    return `${urlApi}authors`;
  },

  getLocations: () => {
      return `${urlApi}locations`;
    }
}
