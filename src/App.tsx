import React from 'react';
import Main from './pages/main';
import {
  BrowserRouter as Router,
  Route,
  Routes
} from "react-router-dom";
import styles from './App.module.scss'
import { store } from './store'
import { Provider } from 'react-redux'
import Xui from './pages/hui/hui';

const App: React.FC = () => {  
  return (
    <Provider store={store}>
      <Router>
        <div className={styles.app__container}>
          <Routes>
            <Route path="/:id" element={<Main />} />
            <Route path="/" element={<Xui />} />
          </Routes>
        </div>
      </Router>
    </Provider>
  );
}

export default App;
