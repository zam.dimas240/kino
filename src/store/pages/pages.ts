import { createSlice } from '@reduxjs/toolkit'

export interface CardsState {
  page: number;
}

const initialState: CardsState = {
  page: 1
}

export const pageSlice = createSlice({
  name: 'pages',
  initialState,
  reducers: {
    increment(state, action) {
      state.page = ++state.page;
    },
    decrement(state, action) {
      state.page = --state.page;
    }
  },
})

export const { increment, decrement } = pageSlice.actions

export default pageSlice.reducer