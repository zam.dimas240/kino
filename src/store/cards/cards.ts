import { createSlice, createAsyncThunk } from '@reduxjs/toolkit'
import axios from 'axios';
import { cardsApi } from '../../api/cardsApi';

export interface CardsState {
  cards: any
  filter: any
  loading: boolean
  locations: any
  authors: any
}

const initialState: CardsState = {
  loading: false,
  filter: {
    name: null,
    authorId: null,
    locationId: null,
    _page: 1,
    _limit: 12,
  },
  cards: [],
  locations: [],
  authors: []
}

export const fetchCardsQuery = createAsyncThunk(
  'cards/fetchCardsQuery', // отображается в dev tools и должно быть уникально у каждого Thunk
  async (queryObj) => {
    console.log(queryObj);
    const response = await axios.get(cardsApi.getCardsQuery(queryObj));
    return response.data;
  }
);

export const fetchAuthors = createAsyncThunk(
  'cards/fetchAuthors', // отображается в dev tools и должно быть уникально у каждого Thunk
  async () => {
    const response = await axios.get(cardsApi.getAuthors());
    return response.data;
  }
);

export const fetchLocations = createAsyncThunk(
  'cards/fetchLocations', // отображается в dev tools и должно быть уникально у каждого Thunk
  async () => {
    const response = await axios.get(cardsApi.getLocations());
    return response.data;
  }
);

export const cardsSlice = createSlice({
  name: 'cards',
  initialState,
  reducers: {
    switchPage(state, action) {
      state.filter._page = action.payload;
    },
    incrementPage(state) {
      state.filter._page = ++state.filter._page;
    },
    decrementPage(state) {
      state.filter._page = --state.filter._page;
    },
    incrementPageMax(state) {
      state.filter._page = 3;
    },
    incrementPageSmall(state) {
      state.filter._page = 1;
    },
    changeFilterName(state, action) {
      state.filter.name = action.payload
    },
    changeFilterAuthor(state, action) {
      state.filter.authorId = action.payload
    },
    changeFilterLocation(state, action) {
      state.filter.locationId = action.payload
    }
  },
  extraReducers: (builder) => {
    builder.addCase(fetchCardsQuery.pending, (state) => {
      state.loading = true;
    })

    builder.addCase(fetchCardsQuery.fulfilled, (state, { payload }) => {
      state.cards = payload;
      state.loading = false;
    })

    builder.addCase(fetchAuthors.pending, (state) => {
      state.loading = true;
    })

    builder.addCase(fetchAuthors.fulfilled, (state, { payload }) => {
      state.authors = payload;
      state.loading = false;
    })

    builder.addCase(fetchLocations.pending, (state) => {
      state.loading = true;
    })

    builder.addCase(fetchLocations.fulfilled, (state, { payload }) => {
      state.locations = payload;
      state.loading = false;
    })
  }
})

export const { changeFilterName, changeFilterAuthor, changeFilterLocation, incrementPageSmall, decrementPage, switchPage, incrementPage, incrementPageMax} = cardsSlice.actions

export default cardsSlice.reducer