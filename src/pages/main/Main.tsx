import React, { useState } from "react";
import Cards from "../../components/Cards";
import FilterSection from "../../components/FilterSection";
import Header from "../../components/Header";
import Pagination from "../../components/Pagination";

const Main = () => {
  return (
    <div>
      <Header />
      <FilterSection />
      <Cards />
      <Pagination />
      {/* <div className={styles.blocks}>
        {ITEMS.map((el) => (
          <div className={styles.block}>
            {el.name}
          </div>
        ))}
      </div> */}
    </div>
  )
}

export default Main;